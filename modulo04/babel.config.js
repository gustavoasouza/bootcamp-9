module.exports = {
    // Definindo quais presets do babel usar
    presets: [
        "@babel/preset-env", // Responsavel pro transpilar ES6 para uma versão que navegadores entendem
        "@babel/preset-react" // Transformar coisas do React que navegador não entende
    ],
    plugins: [
        '@babel/plugin-proposal-class-properties'
    ]
}
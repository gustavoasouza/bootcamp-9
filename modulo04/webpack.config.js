const path = require('path')

module.exports = {
    entry: path.resolve(__dirname, 'src', 'index.js'), // Arquivo de entrada (primeiro arquivo a ser acessado)
    output: {
        path: path.resolve(__dirname, 'public'),
        filename: 'bundle.js'
    }, // Local onde será colocado o Bundle (arquivo transpilado)
    devServer: {
      contentBase: path.resolve(__dirname, 'public')  
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.css$/,
                use: [
                    { loader: 'style-loader' }, // Importar arquivos css
                    { loader: 'css-loader' } // Serve para trazer importações de dentro de arquivos css
                ]
            },
            {
                test: /.*\.(gif|png|jpe?g)$/i,
                use: {
                    loader: 'file-loader'
                }
            }
        ]
    }
}
import React from 'react'
import PropTypes from 'prop-types'

export default function TechItem({ tech, onDelete }) {
    return (
            <li key={tech}>
                {tech}
                {/* 
                    Criando uma arrow function, pois como estamos passando parametros
                    se a função for passada sozinha, ela automaticamente será executada

                    Porém como está dentro de uma arrow function, ela só será executada, quando
                    o usuário clicar no botão
                */}
                <button onClick={onDelete} type="button">Remover</button>
            </li>
    )
}

// defaulttProps é usuado para definir valores padrão quando uma props vier sem valor
TechItem.defaultProps = {
    tech: 'Oculto'
}

// propTypes é uma maneira de validar os dados que são passados por parâmetro em um componente
TechItem.propTypes = {
    tech: PropTypes.string,
    onDelete: PropTypes.func.isRequired // Tipo: Função e Obrigatória
}
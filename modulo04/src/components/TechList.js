import React, { Component } from 'react'

import TechItem from './TechItem'

export default class TechList extends Component {
    // Definindo defaultProps de uma classe
    // static defaultProps = {
    //     tech: 'Oculto'
    // }

    // Definindo propTypes de uma classe
    // static propTypes = {
    //     tech: 'Oculto'
    // }

    state = {
        newTech: '',
        techs: []
    }

    // Executado assim que o componente aparece em tela
    componentDidMount() {
        const techs = localStorage.getItem('techs')

        if (techs) {
            this.setState({ techs: JSON.parse(techs) })
        }
    }

    // Executado sempre que houver alterações nas props ou no estado
    // prevProps - Propriedades antes de serem alteradas
    // prevState - Estado antes de ser alterado
    componentDidUpdate(_, prevState) {
        if (prevState.techs != this.state.techs) {
            localStorage.setItem('techs', JSON.stringify(this.state.techs))
        }
    }

    // Executado quando o componente deixa de existor
    // componentWillUnmount() {}

    // Escrito em forma de arrow function pra permtir que essa função acesse o this
    // Caso contrário, será necessári executar uma arrow function no onChange
    handleInputChange = e => {
        this.setState({ newTech: e.target.value })
    }

    handleSubmit = e => {
        e.preventDefault()

        // States são imutaveis, por tanto a função setState ela gera um novo state
        // Como não podemos modificar diretamente o state, nós geramos um novo array passando o newTech
        this.setState({
            techs: [...this.state.techs, this.state.newTech],
            newTech: ''
        })
    }

    // Quando se criam funções pra manipular o estado, as funções precisam ficar no mesmo 
    // componente em que o estado está
    handleDelete = (tech) => {
        // Retornando todas as tecnologias que sejam diferentes da passada no parametro tech
        this.setState({ techs: this.state.techs.filter(t => t != tech) })
    }

    // Toda vez o estado muda, o render executa novamente
    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h1>{this.state.newTech}</h1>
                <ul>
                    {this.state.techs.map(tech => (
                        <TechItem
                            key={tech}
                            tech={tech}
                            onDelete={() => this.handleDelete(tech)}
                        />
                    ))}
                </ul>
                <input
                    type="text"
                    onChange={this.handleInputChange}
                    value={this.state.newTech}
                />
                <button type="submit">Enviar</button>
            </form>
        )
    }
}

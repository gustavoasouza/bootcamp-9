const express = require('express')

const server = express()

// Aqui estamos dizendo ao express, que ele deve ler json do corpo da requisição
// Caso não possua essa declaração, irá dar erro
server.use(express.json())

// Query params = ?test=1
// Route params = /users/1
// Request body = { "name": "gustavo", "email": "gustavo@hotmail.com" }

// CRUD - Create, read, update, delete
const users = ['Gustavo', 'Guilherme', 'Ezequiel']

// Middleware global
app.use((req, res, next) => {
    // Calculando o tempo de resposta de cada rota
    console.time('Request')

    // Middleware de log, retornando algumas informações de debug da requisição
    console.log(`Método: ${req.method}; URL: ${req.url}`)

    // Permitindo que a s demais rotas sejam executadas
    return next()

    console.timeEnd('Request')
})

// Criando um middleware local
// Para usar um middleware local, pasta colocar ele como segundo parametro após a rota
function checkUserExists(req, res, next) {
    // Se no body não conter uma informação name
    if (!req.body.name) {
        return res.status(400).json({ error: "User not found on request body" })
    }

    // Se ele não entrar no if, segue normal
    return next()
}

// Checando se o usuário está no array
function checkUserInArray(req, res, next) {
    const user = users[req.params.index]

    if (!user) {
        return res.status(400).json({ error: "User not found" })
    }

    // Middlewares podem alterar as variaveis req e res
    // Adicionando uma nova variável dentro do req
    // Agora toda rota que utilizr esse middleware, pode ter acesso a esta variavel user
    req.user = user

    return next()
}

server.get('/users', (req, res) => {
    return res.json(users)
})

server.get('/users/:index', checkUserInArray, (req, res) => {
    return res.json(req.user)
})

server.post('/users', checkUserExists, (req, res) => {
    const { name } = req.body

    users.push(name)

    return res.json(users)
})

server.put('/users/:index',checkUserInArray, checkUserExists, (req, res) => {
    const { index } = req.params
    const { name } = req.body

    users[index] = name

    return res.json(users)
})

server.delete('/users/:index', checkUserInArray, (req, res) => {
    const { index } = req.params

    // Cortar uma posição a partir do index
    users.splice(index, 1)

    return res.send()
})

server.listen(3000)
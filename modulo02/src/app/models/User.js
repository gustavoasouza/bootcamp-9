import Sequelize, { Model } from 'sequelize'
import bcrypt from 'bcryptjs'

class User extends Model {
    // Métodos estáticos são métodos chamados diretamente da própria classe
    // Não pode ser chamados da instância da mesma
    static init(sequelize) {
        // Chamando o método init da classe Model utilizando a função super que aponta pra classe pai
        super.init({
            // Esses campos não precisam ser um reflexo da base de dados
            // São apenas os campos que o usuário poderá preencher
            name: Sequelize.STRING,
            email: Sequelize.STRING,
            password: Sequelize.VIRTUAL, // VIRTUAL é um campo que nunca vai existir na base de dados
            password_hash: Sequelize.STRING,
            provider: Sequelize.BOOLEAN
        },
            {
                sequelize, // Conexão do sequelize
                tableName: 'users' // Forçando nome da tabela
            })

        // Hooks são trechos de código que são executados automaticamente de acordo com ações que acontecem no model
        // beforeSave - Antes de um registro ser salvo, executa algo...
        this.addHook('beforeSave', async (user) => {
            if (user.password) {
                // 1°: O que será convertido em hash
                // 2°: A "força" da hash
                user.password_hash = await bcrypt.hash(user.password, 8)
            }
        })

        // Retornando o model que foi inicializado
        return this
    }

    static associate(models) {
        this.belongsTo(models.File, { foreignKey: 'avatar_id', as: 'avatar' })
    }

    checkPassword(password) {
        return bcrypt.compare(password, this.password_hash)
    }
}

export default User
import jwt from 'jsonwebtoken'
import { promisify } from 'util'

import authConfig from '../../config/auth'

export default async (req, res, next) => {
    const authHeader = req.headers.authorization

    if (!authHeader) {
        return res.status(401).json({ error: 'Token not provided' })
    }

    // Dando um split para isolar a palavra Bearer do token
    const [, token] = authHeader.split(' ')

    try {
        // promisify transforma uma função callback em uma assincrona
        // e o mesmo retorna uma função passando o pametro da função original
        const decoded = await promisify(jwt.verify)(token, authConfig.secret)

        // Permitir que alterações sejam feita por meio do id do usuário sem o mesmo ter que passar o id
        // As rotas que vierem a partir desse middleware de autenticacao, terão acesso a essa variavel
        req.userId = decoded.id

        return next()
    } catch (err) {
        return res.status(401).json({ error: 'Token invalid' })
    }
}
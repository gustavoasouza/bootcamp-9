import User from '../models/User'
import Notification from '../schemas/Notification'

class NotificationController {
    async index(req, res) {
        const checkIsProvider = await User.findOne({
            where: { id: req.userId, provider: true }
        })

        if (!checkIsProvider) {
            return res
                .status(401)
                .json({ error: 'Only providers can load notifications' })
        }

        const notifications = await Notification.find({
            user: req.userId
        })
            .sort({ createdAt: 'desc' })
            .limit(20) // ordenando por meio de chaining

        return res.json(notifications)
    }

    async update(req, res) {
        const notification = await Notification.findByIdAndUpdate(
            req.params.id,
            { read: true },
            { new: true } // Depois de atualizar ele vai retornar a nova notificação atualizada pra poder listar
        )

        return res.json(notification)
    }
}

export default new NotificationController()
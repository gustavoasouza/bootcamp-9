import * as Yup from 'yup'
import User from '../models/User'
import File from '../models/File'

class UserController {
    async store(req, res) {
        // Validando um objeto, e passando o formato que eu quero que o objeto tenha
        const schema = Yup.object().shape({
            name: Yup.string().required(),
            email: Yup.string().email().required(),
            password: Yup.string().required().min(6)
        })

        // Checando se os dados passados pelo body, batem com o modelo do schema
        if (!(await schema.isValid(req.body))) {
            return res.status(400).json({ error: 'Validation fails' })
        }

        const userExists = await User.findOne({ where: { email: req.body.email } })

        // Checando se o usuário já existe
        if (userExists) {
            return res.status(400).json({ error: 'User already exists' })
        }

        // Podemos pegar tudo que está no req.body 
        // pois no model já está explicito tudo que podemos pegar
        // se for passado algo além do que está no model, ele não irá pegar

        // Desestruturando os campos que serão retornados ao frontend
        await User.create(req.body)

        const { id, name, avatar } = await User.findByPk(req.userId, {
            include: [
                {
                    model: File,
                    as: 'avatar',
                    attributes: ['id', 'path', 'url']
                }
            ]
        })

        return res.json({
            id,
            name,
            email,
            avatar
        })
    }

    async update(req, res) {
        const schema = Yup.object().shape({
            name: Yup.string(),
            email: Yup.string().email(),
            oldPassword: Yup.string().min(6),
            password: Yup.string().min(6).when('oldPassword', (oldPassword, field) => 
                oldPassword ? field.required() : field
            ),
            confirmPassword: Yup.string().when('password', (password, field) => 
                password ? field.required().oneOf([Yup.ref('password')]) : field
            )
        })

        if (!(await schema.isValid(req.body))) {
            return res.status(400).json({ error: 'Validation fails' })
        }

        const { email, oldPassword } = req.body

        const { user } = await User.findByPk(req.userId)

        if (email !== user.email) {
            const userExists = await User.findOne({ where: { email } })

            if (userExists) {
                return res.status(400).json({ error: 'User already exists' })
            }
        }

        if (oldPassword && !(await user.checkPassword(oldPassword))) {
            return res.status(401).json({ error: 'Password does not match' })
        }

        const { id, name, provider } = await user.update(req.body)

        return res.json({
            id,
            name,
            email,
            provider
        })
    }
}

export default new UserController()
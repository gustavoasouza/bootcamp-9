import { startOfDay, endOfDay, parseISO, parseISO } from 'date-fns'
import { Op } from 'sequelize'

import Appointment from '../models/Appointment'
import User from '../models/User'

class ScheduleController {
    async index(req, res) {
        const checkUserProvier = await User.findOne({
            where: { id: req.userId, provider: true }
        })

        if (!checkUserProvier) {
            return res.status(400).json({ error: 'User is not a provider' })
        }

        const { date } = req.query
        const parsedDate = parseISO(date)

        const appointments = await Appointment.findAll({
            where: {
                provider_id: req.userId,
                cancelled_at: null,
                date: {
                    [Op.between]: [
                        startOfDay(parsedDate), // Pegando os registros em que a data está entre o começo e o fim do dia
                        endOfDay(parsedDate)
                    ]
                }
            },
            include: [
                {
                    model: User,
                    as: 'user',
                    attributes: ['name']
                }
            ],
            order: ['date']
        })

        return res.json(appointments)
    }
}

export default new ScheduleController()
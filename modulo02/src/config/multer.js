import multer from 'multer'

// Libs padrão do node
import crypto from 'crypto' 
import { extname, resolve } from 'path'

export default {
    // Define a maneira como o multer vai armazenar os arquivos
    storage: multer.diskStorage({
        destination: resolve(__dirname, '..', '..', 'tmp', 'uplouds'), // Destino dos arquivos

        // Como iremos formatar o nome da nossa imagem

        /*
            req - Requisição vinda do express
            file - Contém todas as informações do arquivo
            cb - Callback que será executada contendo o nome do arquivo ou o erro
        */

        filename: (req, file, cb) => {
            // Gerando um nome unico para a imagem
            crypto.randomBytes(16, (err, res) => {
                if (err) return cb(err)

                // Para garantir que não terá caracteres especiais no nome do arquivo
                // será salvo somente o nome random gerado + a extensão
                
                // Null é porque o primeiro parametro do cb é o erro
                return cb(null, res.toString('hex') + extname(file.originalname))
            })
        }
    })
}
/*
    app.listen não foi passado dentro do constructor da classe
    pois quando for feito testes unitários, os testes serão
    feitos direto na classe App, com isso não será necessário
    passar uma porta.
*/

import app from './app'

app.listen(3333)
import express from 'express'
import path from 'path'
import cors from 'cors'
import routes from './routes'

import './database'

/*
    Criando por meio de classes, por questão de organização.
    Por meio da classe, nós damos nomes as coisas (não deixando solto)
*/

class App {
    // Toda vez que a classe é chamada, o constructor é executado
    constructor() {
        this.server = express()

        this.middlewares()
        this.routes()
    }

    // Aqui será cadastrado todos os middlewares da aplicação
    middlewares() {
        this.server.use(express.json())
        this.server.use(cors())
        this.server.use('/files', express.static(path.resolve(__dirname, '..', 'tmp', 'uploads')))
    }

    // Aqui será cadastrado todas as rotas da aplicação
    routes() {
        this.server.use(routes)
    }
}

export default new App().server
/*
  As migrations são como linha do tempo da nossa database
  a partir do momento que já criamos a migration pra tabela
  de usuários, se for necessário alguma alteração, podemos 
  criar uma nova migration com essas alterações.
*/

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      1° paramêtro é a tabela que queremos adicionar a coluna
      2° paramêtro nome da coluna que será adicionada
      3° paramêtro um objeto contendo as informações desse campo
    */
    return queryInterface.addColumn(
      'users',
      'avatar_id',
      {
        type: Sequelize.INTEGER, // Vamos referenciar apenas um id da imagem
        references: { model: 'files', key: 'id' }, // Todo avatar_id na tabela users será o mesmo que id na tabela files
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: true
      }
    )
  },

  down: queryInterface => {
    return queryInterface.removeColumn('users', 'avatar_id')
  }
};

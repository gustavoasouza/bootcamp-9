/*
  Após a tabela ter sido defina, para cria-la
  usa-se o comando: yarn sequelize db:migrate

  Uma tabela SequelizeMeta é criada automaticamente,
  nela estão informações de todas as migrations já executadas

  Caso enquanto esteja desenvolvendo essa migration, e ainda não
  tenha sido enviada para outros devs, e tenha a necessidade de fazer
  alterações, basta usar o comando: yarn sequelize db:migrate:undo ou undo:all
*/

module.exports = {
  // Quando a migration for executada
  up: (queryInterface, Sequelize) => {

    return queryInterface.createTable('users', {
      id: {
        type: Sequelize.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      password_hash: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      provider: {
        type: Sequelize.BOOLEAN,
        defaultValue: false,
        allowNull: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });

  },

  // Se for necessário dar um rollback
  down: (queryInterface, Sequelize) => {

    return queryInterface.dropTable('users');

  }
};

/*
    Redux Saga são middlewares que executam efeitos coleterais
    baseado em actions disparadas.

    Nesse primeiro caso quando a addToCard for disparada, iremos
    buscar na API os dados restantes que faltam no produto.

    O * na função indica que ela é uma Generator Function, é a mesma
    coisa que async functions porém são mais poderosas.
*/

// Call é responsavel por chamar métodos assincronos e que retornam Promises
// Put dispara uma action dentro do saga
import { call, select, put, all, takeLatest } from 'redux-saga/effects'
import { toast } from 'react-toastify'

import api from '../../../services/api'
import history from '../../../services/history'
import { formatPrice } from '../../../util/format'

import { addToCartSuccess, updateAmountSuccess } from './actions'

function* addToCart({ id }) {
    const productExists = yield select(
        state => state.cart.find(p => p.id == id)
    )

    const stock = yield call(api.get, `/stock/${id}`)

    const stockAmount   = stock.data.amount
    const currentAmount = productExists ? productExists.amount : 0

    const amount = currentAmount + 1

    if (amount > stockAmount) {
        toast.error('Quantidade solicitada fora de estoque')
        return // Dando um return vazio pro resto da função não continuar
    }

    if (productExists) {
        yield put(updateAmountSuccess(id, amount))
    } else {
        // yield é como se fosse o await do Generator
        const response = yield call(api.get, `/products/${id}`)

        const data = {
            ...response.data,
            amount: 1,
            priceFormatted: formatPrice(response.data.price)
        }

        yield put(addToCartSuccess(data))
        history.push('/cart')
    }
}

function* updateAmount({ id, amount }) {
    if (amount <= 0) return

    const stock = yield call(api.get, `stock/${id}`)
    const stockAmount = stock.data.amount

    if (amount > stockAmount) {
        toast.error('Quantidade solicitada fora de estoque')
        return
    }

    yield put(updateAmountSuccess(id, amount))
}

export default all([
    // Se o usuário clicar duas vezes no botão, o saga só utilizará o ultimo clique
    // se fosse o takeEvery, todos os cliques seriam adicionados
    takeLatest('@cart/ADD_REQUEST', addToCart),
    takeLatest('@cart/UPDATE_AMOUNT_REQUEST', updateAmount)
])
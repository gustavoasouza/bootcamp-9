import { combineReducers } from 'redux'

import cart from './cart/reducer'

// Combinando reducers
export default combineReducers({
    cart
})
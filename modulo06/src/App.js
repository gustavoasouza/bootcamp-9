import React from 'react';
import { Router } from 'react-router-dom'
import { Provider } from 'react-redux'
import { ToastContainer } from 'react-toastify'

import './config/ReactotronConfig'

import GlobalStyle from './styles/global'
import Header from './components/Header'
import Routes from './routes'

import history from './services/history'
import store from './store'

function App() {
  return (
    // O provider é quem permite que nosso store seja acessivel em toda a aplicação
    <Provider store={store}>
      <Router history={history}>
        {/* 
          Como o Header também precisa ter acessado as rotas, ele precisa
          ficar envolvido pelo BrowserRouter
        */}
        <Header />
        <Routes />
        <GlobalStyle />
        <ToastContainer autoClose={3000} />
      </Router>
    </Provider>
  );
}

export default App;

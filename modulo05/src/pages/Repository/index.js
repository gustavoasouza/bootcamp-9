import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import api from '../../services/api'

import Container from '../../components/Container'
import { Loading, Owner, IssueList } from './styles'

export default class Repository extends Component {
    static propTypes = {
        match: PropTypes.shape({
            params: PropTypes.shape({
                repository: PropTypes.string
            })
        }).isRequired
    }

    state = {
        repository: {},
        issues: [],
        loading: true
    }

    async componentDidMount() {
        const { match } = this.props

        const repoName = decodeURIComponent(match.params.repository)

        // Como são duas requisições que precisam ser carregadas juntas, usa-se
        // o Promise.all para executar ambas ao mesmo tempo, ou seja, a próxima linha do código
        // só será executada quando as duas requisições terminaram

        // Como é um array, o 0 é o primeiro retorno e 1 o segundo, porém pode ser feito uma
        // desestruturação e atribuir nomes aos valores, no caso repository e issues
        const [repository, issues] = await Promise.all([
            api.get(`/repos/${repoName}`),
            api.get(`/repos/${repoName}/issues`, {
                state: 'open',
                per_page: 5
            }) // Terceiro paramêtro são os query params da requisição
        ])

        this.setState({
            repository: repository.data,
            issues: issues.data,
            loading: false
        })
    }

    render() {
        const { repository, issues, loading } = this.state

        if (loading) {
            return <Loading>Carregando</Loading>
        }

        return (
            <Container>
                <Owner>
                    <Link to="/">Voltar aos repositórios</Link>
                    <img src={repository.owner.avatar_url} alt={repository.owner.login} />
                    <h1>{repository.name}</h1>
                    <p>{repository.description}</p>
                </Owner>

                <IssueList>
                    {issues.map(issue => (
                        // Uma boa prática é sempre passar a key como sendo uma string
                        <li key={String(issue.id)}>
                            <img src={issue.user.avatar_url} alt={issue.user.login} />
                            <div>
                                <strong>
                                    <a href={issue.html_url}>{issue.title} </a>
                                    {issue.labels.map(label => (
                                        <span key={String(label.id)}>{label.name}</span>
                                    ))}
                                </strong>
                                <p>{issue.user.login}</p>
                            </div>
                        </li>
                    ))}
                </IssueList>
            </Container>
        )
    }
}

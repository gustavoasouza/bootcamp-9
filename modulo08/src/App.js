import React, { useState, useEffect, useMemo, useCallback } from 'react';

function App() {
  // Com o useState (Hooks) cada informação na aplicação terá seu estado separado
  const [tech, setTech] = useState([])
  const [newTech, setNewTech] = useState('')

    /*
      useCallback tem a mesma função do useMemo porém ao invés de retornar um valor
      unico, ele retorna uma função.
    */
  const handleAdd = useCallback(() => {
    setTech([...tech, newTech])
    setNewTech('')
  }, [newTech, tech])

  /*
    Rodando o useEffect uma unica vez pois não
    tem nenhuma variável sendo monitorada.
  */
  // Representando o componentDidMount
  useEffect(() => {
    const storageTech = localStorage.getItem('techs')

    if (storageTech) {
      setTech(JSON.parse(storageTech))
    }

    // Sempre que quiser simular um componentWillUmount, basta retornar uma função
    // return () => {}
  }, [])

  // Representando componentDidUpdate
  useEffect(() => {
    localStorage.setItem('techs', JSON.stringify(tech))
  }, [tech])

  /*
    Sempre que o component renderizar novamente, ele iria executar o tech.length,
    porém é desnecessário visto que só precisaria executar essa função caso alguma
    tecnologia fosse adicionada.

    O useMemo serve para executar uma função baseado na alteração de alguma variavel.
  */
  const techSize = useMemo(() => tech.length, [tech])

  return (
    <>
      <ul>
        {tech.map(t => <li key={t}>{t}</li>)}
      </ul>
      <stron>Você tem {techSize} tecnologias</stron>
      <input value={newTech} onChange={e => setNewTech(e.target.value)} />
      <button onClick={handleAdd}>Adiconar</button>
    </>
  )
}

export default App;

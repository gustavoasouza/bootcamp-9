// These modules overwrite the core of bable inside the create-react-app
// this allow us to change default configurarion

// Such as add babel plugins

// babel-plugin-root-import allow us to simplify the import using ~ instead dots notation
// to navigate even the page

// if you are using eslint, install the
// eslint-import-resolver-babel-plugin-root-import plugin to ignore these sintax

const { addBabelPlugin, override } = require('customize-cra')

module.exports = override(
    addBabelPlugin([
        'babel-plugin-root-import',
        {
            rootPathSuffix: 'src'
        }
    ])
)
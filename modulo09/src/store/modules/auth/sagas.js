import { takeLatest, call, put, all } from "redux-saga/effects";

import history from "../../../services/history";
import api from "../../../services/api";

import { signInSuccess } from "./actions";

// generator function
export function* signIn({ payload }) {
	const { email, password } = payload;

	// call is used when a method that is async is called
	const response = yield call(api.post, "sessions", {
		email,
		password,
	});

	const { token, user } = response.data;

	if (!user.provider) {
		console.tron.error("Usuário não é prestador");
		return;
	}

	yield put(signInSuccess(token, user));

	history.push("/dashboard");
}

// excute signIn function when @auth/SIGN_IN_REQUEST is executed
export default all([takeLatest("@auth/SIGN_IN_REQUEST", signIn)]);
